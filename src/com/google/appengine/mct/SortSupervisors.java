package com.google.appengine.mct;

import java.util.Comparator;

public class SortSupervisors implements Comparator {

	public int compare(Object o1, Object o2) {
		Supervisor u1 = (Supervisor) o1;
		Supervisor u2 = (Supervisor) o2;
		return u1.getEmailAddress().compareTo(u2.getEmailAddress());
	}
}
