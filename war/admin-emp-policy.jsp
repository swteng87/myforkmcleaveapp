<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>
<%@page import="com.google.appengine.mct.*"%>
<%
String content = (String)request.getAttribute("content");  if (content == null) { content = ""; }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  
  <head>
    <title>Master Concept</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style>
    	a{text-decoration:none !important;}
    	
    
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    
    
    <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-wysihtml5.css"></link>
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <style>
      undefined
    </style>
     <script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.js"> </script>
    <script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
      <script src="assets/js/wysihtml5-0.3.0.js"></script>
      <script src="assets/js/bootstrap-wysihtml5.js"></script>
    <script src="assets/js/prettify.js"></script>
    <script src="assets/js/common.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
        	
        	$(prettyPrint);
        	
        	$('.textarea').wysihtml5();
        	$('.textarea').val('<%=content.replaceAll("\\r\\n|\\r|\\n"," ")%>');	
        	
    		$("#editorDiv").hide();
    		$("#updatePolicy").hide();
    		$("#backPolicy").hide();
    		
    		$("#backPolicy").on("click",function(){
    			$("#editorDiv").hide();
        		$("#updatePolicy").hide();
        		$("#backPolicy").hide();
        		$("#companyPolicy").show();
        		$("#editPolicy").show();
    		});
    		
        	$("#editPolicy").on("click",function(){
        		$("#editorDiv").show();
        		$("#updatePolicy").show();
        		$("#backPolicy").show();
        		$("#companyPolicy").hide();
        		$("#editPolicy").hide();
        	});
        	
        	$("#updatePolicy").click(function(){
        		
        		var content = $('.textarea').val();
        		if(content != "" && content != "undefine"){
            		$(".span9").block({
    	    		 	showOverlay: true, 
    	    	        centerY: true, 
    	    	        css: { 
    	    	            width: '200px',  
    	    	            border: 'none', 
    	    	            padding: '15px', 
    	    	            backgroundColor: '#000', 
    	    	            '-webkit-border-radius': '10px', 
    	    	            '-moz-border-radius': '10px', 
    	    	            opacity: .6, 
    	    	            color: '#fff' 
    	    	        }, 
    	    			message: '<font face="arial" size="4">Loading ...</font>'
    	    			
    	    		});
        		
            		
    	    		
    	    		$.ajax({
    	    			type: "POST",
    	    			url: "/UpdateCompanyPolicy",
    	    			data: {content:content},
    	    			success: function(response) {
    	    				$(".span9").unblock();
    	    				$("#editorDiv").hide();
    	    				$("#updatePolicy").hide();
    	    	    		$("#backPolicy").hide();
    	    	    		$("#editPolicy").show();
    	    				$("#companyPolicy").show();
    	                	$("#companyPolicy").html(response);
    	                	
    	                	return false;
    	    			}
    	    		});
            	}
        		else{
        			$('#myModal').modal('show');
        		}
            	
    				
            	});
        	
        	
        });
    </script>
  </head>
  
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
    <!-- <div class="well sidebar-nav"> -->
<!--     <ul class="nav nav-list"> -->
    <jsp:include page="leave-menu.jsp"></jsp:include>
<!-- 	</ul> -->
    <!-- </div> -->
    </div>
    <div class="span9" >
    <div class="row-fluid">
    <div id="content">
    <h5 id="Leave Requests Admin">LEAVE POLICY</h5>
    <hr>
    			<span>
					<a href="#" id="editPolicy" class=" btn btn-primary">
					<i class="icon-white icon-edit"></i> Edit</a>
				</span>
				<span>
					<a href="#" id="backPolicy" class=" btn btn-danger">
					<i class="icon-white icon-chevron-left"></i> Back</a>
				</span>
				<span>
					<a href="#" id="updatePolicy" class=" btn btn-primary">
					<i class="icon-white icon-edit"></i> Save</a>
				</span>
				<br><br>
				<div id="editorDiv">
				<div class="hero-unit">
				<h4>Editor Company Policy</h4>
				<hr/>
				<textarea class="textarea" placeholder="Enter text ..." style="width: 810px; height: 200px"></textarea>
				</div>
				
<!-- 				<div id="alerts"></div> -->
<!--     <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor"> -->
<!--       <div class="btn-group"> -->
<!--         <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="icon-font"></i><b class="caret"></b></a> -->
<!--           <ul class="dropdown-menu"> -->
<!--           </ul> -->
<!--         </div> -->
<!--       <div class="btn-group"> -->
<!--         <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a> -->
<!--           <ul class="dropdown-menu"> -->
<!--           <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li> -->
<!--           <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li> -->
<!--           <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li> -->
<!--           </ul> -->
<!--       </div> -->
<!--       <div class="btn-group"> -->
<!--         <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a> -->
<!--         <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a> -->
<!--         <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="icon-strikethrough"></i></a> -->
<!--         <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a> -->
<!--       </div> -->
<!--       <div class="btn-group"> -->
<!--         <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="icon-list-ul"></i></a> -->
<!--         <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="icon-list-ol"></i></a> -->
<!--         <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a> -->
<!--         <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="icon-indent-right"></i></a> -->
<!--       </div> -->
<!--       <div class="btn-group"> -->
<!--         <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a> -->
<!--         <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a> -->
<!--         <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a> -->
<!--         <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a> -->
<!--       </div> -->
<!--       <div class="btn-group"> -->
<!-- 		  <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="icon-link"></i></a> -->
<!-- 		    <div class="dropdown-menu input-append"> -->
<!-- 			    <input class="span2" placeholder="URL" type="text" data-edit="createLink"/> -->
<!-- 			    <button class="btn" type="button">Add</button> -->
<!--         </div> -->
<!--         <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="icon-cut"></i></a> -->

<!--       </div> -->
      
<!--       <div class="btn-group"> -->
<!--         <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="icon-picture"></i></a> -->
<!--         <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" /> -->
<!--       </div> -->
<!--       <div class="btn-group"> -->
<!--         <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a> -->
<!--         <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a> -->
<!--       </div> -->
<!--       <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech=""> -->
<!--     </div> -->
<!-- 	<div id="editor"> -->
<!--       Go ahead&hellip; -->
<!--     </div> -->
  </div>
				
				<div id="companyPolicy"><%=content %></div>
				
<!-- 				<div> -->
<!-- 				<table border="0" style="color:black;" cellpadding="5" > -->
<!-- 					<tr> -->
<!-- 						<td style="padding-left:0px;" colspan="2"><font size="2">ANNUAL LEAVE</font></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Employee is entitled to annual leave as shown as your employment contract.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Annual Leave cannot be brought forward to next financial year - 1 April to 31 March.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>There are no special approval for taking annual leave under probation period.</td> -->
<!-- 					</tr> -->
<!-- 					<tr><td></td></tr><tr><td></td></tr> -->
					
<!-- 					<tr> -->
<!-- 						<td style="padding-left:0px;" colspan="2"><font size="2">SICK LEAVE</font></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Employee must inform his/her direct supervisor and Admin Department.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Doctor Letter / Receipt must be provided for sick leave one day or over.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Employee must submit sick leave application with doctor letter to HR after back to office.</td> -->
<!-- 					</tr> -->
<!-- 					<tr><td></td></tr><tr><td></td></tr> -->
					
<!-- 					<tr> -->
<!-- 						<td style="padding-left:0px;" colspan="2"><font size="2">COMPENSATION LEAVE</font></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>All staffs can be entitled when working during holidays (Saturdays, Sundays & Public Holidays). Entitlement application must be submitted and approved before apply compensation leave.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Must be approved by direct supervisor.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Must be redeemed within 3 months or it will be forfeited automatically.</td> -->
<!-- 					</tr> -->
<!-- 					<tr><td></td></tr><tr><td></td></tr> -->
					
<!-- 					<tr> -->
<!-- 						<td style="padding-left:0px;" colspan="2"><font size="2">COMPASSIONATE LEAVE</font></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Staffs are allowed 3 Days Paid Leave for the death and making funeral arrangement of immediate family members.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>The definition of immediate family member would refer under Labour Law (if any).</td> -->
<!-- 					</tr> -->
<!-- 					<tr><td></td></tr><tr><td></td></tr> -->
					
<!-- 					<tr> -->
<!-- 						<td style="padding-left:0px;" colspan="2"><font size="2">NO PAY LEAVE</font></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>The start date and end date inclusive, including all holidays.</td> -->
<!-- 					</tr> -->
<!-- 					<tr><td></td></tr><tr><td></td></tr> -->
					
<!-- 					<tr> -->
<!-- 						<td style="padding-left:0px;" colspan="2"><font size="2">MATERNITY LEAVE</font></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Refer to the Law accordingly.</td> -->
<!-- 					</tr> -->
<!-- 					<tr><td></td></tr><tr><td></td></tr> -->
					
<!-- 					<tr> -->
<!-- 						<td style="padding-left:0px;" colspan="2"><font size="2">BIRTHDAY LEAVE</font></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Every employee is eligible for 1 day leave on his / her birthday. This leave will be credited annually.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding-left:0px;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Entitlement of such leave can only be taken 1 day before the birthdate or 1 day after the birthdate or on the birthdate itself.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding-left:0px;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>The leave cannot be taken under probation period.</td> -->
<!-- 					</tr> -->
<!-- 					<tr><td></td></tr><tr><td></td></tr> -->
					
<!-- 					<tr> -->
<!-- 						<td style="padding-left:0px;" colspan="2"><font size="2">AMENDMENT FOR APPLIED LEAVE</font></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Leave applied and approved but not taken, may be amended as in the number of days or desired leave dates.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Amendment / Cancel For Applied Leave (must fill in the remark field for the reason to amend / cancel the leave).</td> -->
<!-- 					</tr> -->
<!-- 					<tr><td></td></tr><tr><td></td></tr> -->
					
<!-- 					<tr> -->
<!-- 						<td style="padding-left:0px;" colspan="2"><font size="2">WEDDING LEAVE</font></td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Employee having been employed by the Company and completed the probation period satisfactorily are entitled to have 3 days leave.</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td width="10" style="padding:0 0 0 0;"><img src="images/bullet.png"/></td> -->
<!-- 						<td>Employee should submit the Leave Application Form with valid marriage documents to HR Department at least one month before.</td> -->
<!-- 					</tr> -->
<!-- 				</table> -->
<!-- 				</div> -->
				
				</div>
    </div>
    </div>
    
    </div>
    </div>
    
    <!-- Modal -->
			<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			    <h4 id="myModalLabel">Company Policy</h4>
			  </div>
			  <div class="modal-body">
			    <p>Company Policy is mandaatory..</p>
			  </div>
			  <div class="modal-footer">			    
			    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
			  </div>
			</div>
    
	</body>
</html>