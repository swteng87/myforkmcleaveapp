<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>
<%@page import="com.google.appengine.util.*"%>
<%
String addAnnualLeave = (String) request.getAttribute("addAnnualLeave"); 
if (addAnnualLeave == null) { addAnnualLeave = ""; }

String addSickLeave = (String) request.getAttribute("addSickLeave"); 
if (addSickLeave == null) { addSickLeave = ""; }

String addMaternityLeave = (String) request.getAttribute("addMaternityLeave"); 
if (addMaternityLeave == null) { addMaternityLeave = ""; }

String addBirthdayLeave = (String) request.getAttribute("addBirthdayLeave");
if (addBirthdayLeave == null) { addBirthdayLeave = ""; }

String addWeddingLeave = (String) request.getAttribute("addWeddingLeave"); 
if (addWeddingLeave == null) { addWeddingLeave = ""; }

String addCompassionateLeave = (String) request.getAttribute("addCompassionateLeave"); 
if (addCompassionateLeave == null) { addCompassionateLeave = ""; }

String compensationLeaveExp = (String) request.getAttribute("compensationLeaveExp"); 
if (compensationLeaveExp == null) { compensationLeaveExp = ""; }

String hospitalization = (String) request.getAttribute("hospitalization"); 
if (hospitalization == null) { hospitalization = ""; }


String region = (String) request.getAttribute("region"); if (region == null) { region = ""; }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
     <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png"/>
    
    <script src="media/js/jquery.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
	<script src="assets/js/common.js"></script>
		
		<script type="text/javascript">
        $(document).ready(function () {
        	function Go() {
  				if(allMandatory() == true){
  					$('#myModal').modal('show')
  					return false;
  				}
  			    return true;
			}
        	
        	function allMandatory() {
				var bool = false;
        		
        		$('input, select').each(function() {
                     if(this.value == "" || this.value == "undefined"){
                     	bool = true;
                     }
                });
        		
        		return bool;
		 	}
        	
        	
        	$("#save-leaveEntitle").on("click",function(){
        		
        	if(Go()){
        		$(".span9").block({
	    		 	showOverlay: true, 
	    	        centerY: true, 
	    	        css: { 
	    	            width: '200px',  
	    	            border: 'none', 
	    	            padding: '15px', 
	    	            backgroundColor: '#000', 
	    	            '-webkit-border-radius': '10px', 
	    	            '-moz-border-radius': '10px', 
	    	            opacity: .6, 
	    	            color: '#fff' 
	    	        }, 
	    			message: '<font face="arial" size="4">Loading ...</font>'
	    			
	    		});
        		
        		var data = $("form").serialize();
	    		
	    		$.ajax({
	    			type: "POST",
	    			url: "/AddLeaveEntitle",
	    			data: data,
	    			success: function(response) {
	    				$(".span9").unblock();
	    				
	                	$("#msg").html(response);
	                	
	                	return false;
	    			}
	    		});
        	}
        	
				
        	});
				
        	$("#add-sick-leave").on("click",function(){
        		 $.get("sick-leave.jsp", function (response) {
        			 $("#sick-leave").append(response);
                 });
        		
        	});
        	
        });
		</script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
<!--     <div class="well sidebar-nav"> -->
    <jsp:include page="configuration-menu.jsp"></jsp:include>
<!--     </div> -->
    </div>
    <div class="span9">
				<h5 id="Leave Entitle">Add Leave Entitle</h5>
				<hr></hr>
				<div id="msg" class="error-msg"></div>
				<form name="Update" method="post" action="AddLeaveEntitle">
					<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
					<jsp:setProperty name="jspbeans" property="*" />
					<table cellpadding="5" border="0">
						<%//=jspbeans.listSettings()%>
						<tr>
							<td>Entitled Annual Leave</td>
							<td><input type="text" name="addAnnualLeave" value="<%=addAnnualLeave %>" maxlength="4"/></td>
						</tr>
						
						<tr>
							<td>Maternity Leave</td>
							<td><input type="text" name="addMaternityLeave" value="<%=addMaternityLeave %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Birthday Leave</td>
							<td><input type="text" name="addBirthdayLeave" value="<%=addBirthdayLeave %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Wedding Leave</td>
							<td><input type="text" name="addWeddingLeave" value="<%=addWeddingLeave %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Compassionate Leave</td>
							<td><input type="text" name="addCompassionateLeave" value="<%=addCompassionateLeave %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Compensation Entitlement Leave Expire</td>
							<td><input type="text" name="compensationLeaveExp" value="<%=compensationLeaveExp %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td>Hospitalization</td>
							<td><input type="text"  name="hospitalization" value="<%=hospitalization %>" maxlength="4"/></td>
						</tr>
						<tr>
							<td style="padding-bottom:16px;">Region <span class="error-msg"></span></td>
							<td width="230px">
								<div class="dropdown">
									<select name="region">
										<%=jspbeans.listRegion()%>
									</select>
									<script>
										this.document.forms[0].region.value="<%=region%>"; 
									</script>
								</div>
							</td>
							<td></td>
						</tr>
						<tr>
							<td colspan="10">
								<fieldset>
								<legend style="font-size:14px;"><b>Sick Leave</b></legend>
								<table width="450px">
									<tr>
										<td colspan="2">
											<span>
												Sick Leave Entitle
												<input type="text" name="sickLeaveDay[]" value="" placeholder="day" class="input-mini" maxlength="4"/>
											
												<select name="sickLeaveType[]" style="width: 160px">
													<option value="<%=ConstantUtils.LESS_THAN %>">less than</option>
													<option value="<%=ConstantUtils.LESS_THAN_OR_EQUAL %>">less than or equal</option>
													<option value="<%=ConstantUtils.GREATER_THAN %>">greater than</option>
													<option value="<%=ConstantUtils.GREATER_THAN_OR_EQUAL %>">greater than or equal</option>
												</select>
												<input type="text" class="input-mini" name="sickLeaveYear[]" placeholder="year" value="" maxlength="4"/>
												
											</span>
											<div id="sick-leave"></div>
											<div><a href="#" id="add-sick-leave" class="btn btn-small btn-inverse">Add More</a></div>
										</td>
									</tr>
									
								</table>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<hr></hr>
								<a href="#" id="save-leaveEntitle" class="pull-right btn btn-primary">Save</a>
							</td>
						</tr>
					</table>
				</form>
			</div>
			</div></div>
			<!-- Modal -->
				<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
				    <h4 id="myModalLabel">Add Leave Entitle</h4>
				  </div>
				  <div class="modal-body">
				    <p>Please make sure all fields are filled.</p>
				  </div>
				  <div class="modal-footer">			    
				    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
				  </div>
				</div>
	</body>
</html>