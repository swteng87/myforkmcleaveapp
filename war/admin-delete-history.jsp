<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<%
String cri_region = (String) request.getAttribute("cri_region"); if (cri_region == null) { cri_region = ""; }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="utf-8" http-equiv="encoding"/>
		<title>Master Concept Admin</title>
		<!-- <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" /> -->
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    	<meta name="description" content=""/>
   	 	<meta name="author" content=""/>
   	 	
   	 	<!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png"/>
    <style>
	      	/* 
			Max width before this PARTICULAR table gets nasty
			This query will take effect for any screen smaller than 760px
			and also iPads specifically.
			*/
		@media 
		only screen and (max-width: 760px),
		(min-device-width: 768px) and (max-device-width: 1024px)  {
		
			/* Force table to not be like tables anymore */
			#historyList table,
			#historyList thead, 
			#historyList tbody, 
			#historyList th,
			#historyList td, 
			#historyList tr { 
				display: block; 
			}
			
			/* Hide table headers (but not display: none;, for accessibility) */
			#historyList thead tr { 
				position: absolute;
				top: -9999px;
				left: -9999px;
			}
			
	/* 		#employeeList tr { border: 1px solid #ccc; } */
			
			#historyList td { 
				/* Behave  like a "row" */
	/*  		border: none;  */
	/* 			border-bottom: 1px solid #eee;  */
				position: relative;
				padding-left: 50%; 
				white-space: normal;
				text-align:left;
			}
			
			#historyList td:before { 
				/* Now like a table header */
				position: absolute;
				/* Top/left values mimic padding */
				top: 6px;
				left: 6px;
				width: 45%; 
				padding-right: 10px; 
				white-space: nowrap;
				text-align:left;
			}
			
			/*
			Label the data
			*/
			td:nth-of-type(1):before { content: "Select"; } 
			td:nth-of-type(2):before { content: "Time"; } 
	 		td:nth-of-type(3):before { content: "Employee"; } 
	 		td:nth-of-type(4):before { content: "Number of Days"; } 
	 		td:nth-of-type(5):before { content: "Start Date"; } 
	 		td:nth-of-type(6):before { content: "End Date"; } 
	 		td:nth-of-type(7):before { content: "Supervisor"; } 
	 		td:nth-of-type(8):before { content: "Leave Type"; } 
	 		td:nth-of-type(9):before { content: "Change Type"; } 
	 		td:nth-of-type(10):before { content: "Remark"; } 
	/* 		#employeeList td:before { content: attr(data-title); } */
		}
		
		/* Smartphones (portrait and landscape) ----------- */
		@media only screen
		and (min-device-width : 320px)
		and (max-device-width : 480px) {
			body { 
				padding: 0; 
				margin: 0; 
				width: 320px; }
			}
		
		/* iPads (portrait and landscape) ----------- */
		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
			body { 
				width: 495px; 
			}
		}
    	</style>
    	<link rel="stylesheet" type="text/css" href="assets/css/datatable-bootstrap.css"/>
    	<script src="media/js/jquery.js" type="text/javascript"></script>
    	<script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
		<script src="assets/js/bootstrap.js"></script>
   		<script src="assets/js/datatable-bootstrap.js"></script>
		<script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
		<script src="assets/js/common.js"></script>
	
		<script language="JavaScript">
			function Go() {
				if( $('form[name=Delete] input[name=delHisList]:checked').length > 0)
		    	{
		        	$('form[name=Delete]').submit();
		    	}
		    	else
		    	{
		        	/* alert("Please select at least one to delete"); */
		        	$('#myModal').modal('show')
		    	}
			}
			function toggle(id) {
    			var n = document.getElementById(id);
    			n.style.display =  (n.style.display != 'none' ? 'none' : '' );
  			}
			function cmd_parm() { 
				var f = document.forms[0];
				f.submit();
			}
			function cmd_msg(msg) { 
				var f = document.forms[0];
				f.cmd2.value = msg;
				f.submit();
			}
			function clearForm() {
  				var f_elements = document.forms[0].elements;
  				for (i = 0; i < f_elements.length; i++) {
    				field_type = f_elements[i].type.toLowerCase();
    				switch (field_type) {
    					case "text":
    					case "password":
						case "textarea":
    					case "hidden":
    						f_elements[i].value = "";
        					break;
    					case "radio":
    					case "checkbox":
        					if (f_elements[i].checked) {
            					f_elements[i].checked = false;
        					}
        					break;
    					case "select-one":
    						f_elements[i].selectedIndex = 0;
    						break;
    					case "select-multi":
        					f_elements[i].selectedIndex = -1;
        					break;
    					default:
        					break;
    				}
				}
  			}
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
		<!-- <link href="media/css/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="media/css/demo_table.css" rel="stylesheet" type="text/css" />
        <link href="media/css/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="all" />
        <link href="media/css/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" media="all" />
        <link href="media/themes/smoothness/jquery-ui-1.8.4.custom.css" rel="stylesheet" type="text/css" media="all" />
        <script src="media/js/jquery.js" type="text/javascript"></script>
        <script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script> -->
        <script type="text/javascript">
        $(document).ready(function () {
        	
        	var region = "";
        	$("#headed").hide();
        	$("#footed").hide();
            $("#region").on("change", function (){
            	
            	 region = $(this).val();
            	
            	$("#headed").show();
            	$("#footed").show();
            	
            	var oTable = $("#historyList").dataTable({
                    "bServerSide": true,
                    "sAjaxSource": "/DeleteHistory",
                    "bProcessing": true,
                    "bRetrieve": true,
                    "fnServerParams": function ( aoData ) {
                        aoData.push( { "name":"cri_region", "value":region } );
                      },
                    "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
                	"sPaginationType": "bootstrap",
                	"oLanguage": {
                		"sLengthMenu": "_MENU_ records per page"
                	}
                    /* "sPaginationType": "full_numbers",
                    "bJQueryUI": true */
                });
            	// redraw to re bind action in datatable
            	oTable.fnDraw();
            	
            	return;
            	
            });
            
			$("#delete-history").on("click",function(){
    			
    			if( $('form[name=Delete] input[name=delHisList]:checked').length > 0)
		    	{
    				var delHisList = new Array();
    				
        		$("input[name=delHisList]:checked").each(function() { 
        			delHisList.push($(this).val());
        	    });
        		
        		$(".span9").block({
        		 	showOverlay: true, 
        	        centerY: true, 
        	        css: { 
        	            width: '200px',  
        	            border: 'none', 
        	            padding: '20px', 
        	            backgroundColor: '#000', 
        	            '-webkit-border-radius': '10px', 
        	            '-moz-border-radius': '10px', 
        	            opacity: .6, 
        	            color: '#fff' 
        	        }, 
        			message: '<font face="arial" size="4">Loading ...</font>'
        			
        		});
        		
        		$.ajax({
        			type: "POST",
        			url: "/DeleteHistory",
        			data: {delHisList:delHisList},
        			success: function(response) {
        				$(".span9").unblock();
                    	
                    	window.location.href = "admin-delete-history.jsp";
                    	
                    	return false;
        			}
        		});

		    	}
		    	else
		    	{
		        	/* alert("Please select at least one to delete"); */
		        	$('#myModal').modal('show')
		    	}

    		});
        });
        </script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      		<div class="navbar-inner">
      			<div class="container">
      				<jsp:include page="top-menu.jsp"></jsp:include>
      			</div>
      		</div>
    	</div>
		<div class="container-fluid">
    		<div class="row-fluid">
    			<div class="span3" >
    				<!-- <div class="well sidebar-nav"> -->
    						<jsp:include page="account-menu.jsp"></jsp:include>
    				<!-- </div> -->
    			</div>
		<div class="span9">
	        <!-- <div id="head"></div>
			<div id="content"> -->
				<h5 id="Leave History">LEAVE HISTORY</h5>
				<hr></hr>
				<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
				<jsp:setProperty name="jspbeans" property="*"/>
				<form name="Delete" method="post" action="DeleteHistory">
					<input type="hidden" name="cmd" value=""/>
					<input type="hidden" name="cmd2" value=""/>
					<!-- <table width="800px" cellpadding="5" style="color:black; border-top:1px solid #ccc;" border="0"> -->
						<!-- <tr>
							<td colspan="5" style="padding-left:0px; color:black;"><i>To view, please select the region to narrow down your search.</i></td>
						</tr> -->
						<div>
							<i>To view, please select the region to narrow down your search.</i>
						</div>
						<div>
						<!-- <tr>
							<td align="left" style="padding-left:0px;padding-bottom:15px;" width="60px">Region</td>
							<td colspan="2"> -->
								<div class="dropdown">Region
									<select name="cri_region" id="region">
										<%=jspbeans.listRegion()%>
									</select>
									<script>
										this.document.forms[0].cri_region.value="<%=cri_region%>"; 
									</script>
								</div>
						</div>
							<!-- </td>
						</tr>
					</table> -->
					<!-- <div id="container">
					<div id="demo_jui"> -->
					<table id="historyList" cellpadding="0" cellspacing="0"
					class="table table-striped table-bordered" >
					<thead id="headed">
					<tr>
						<th style="width:60px; align:center;" title="Select">Select</th>
						<th align="center" title="Time">Time</th>
						<th align="center" title="Employee">Employee</th>
						<th style="width:110px; align:center;" title="Number of Days">Number of Days</th>
						<th style="width:100px; align:center;" title="Start Date">Start Date</th>
						<th style="width:100px; align:center;" title="End Date">End Date</th>
						<th align="center" title="Supervisor">Supervisor</th>
						<th align="center" title="Leave Type">Leave Type</th>
						<!-- <th align="center" title="Change Type">Change Type</th>
						<th align="center" title="Remark">Remark</th> -->
						</tr>
					</thead>
					<tbody></tbody>
					</table>
					
					<table id="footed">
						<tr>
							<!-- <td colspan="4">
								<a href="#"><img src="images/Delete.png" style="border:none;" onClick="javascript:Go()"
								onmouseover="this.src='images/Delete_Hover.png'" onmouseout="this.src='images/Delete.png'"/></a>
								&nbsp;&nbsp;&nbsp;
								<a href="#"><img src="images/Reset.png" style="border:none;" name="reset" onClick="javascript:clearForm()"
								onmouseover="this.src='images/Reset_Hover.png'" onmouseout="this.src='images/Reset.png'"/></a>
							</td> -->
							<td colspan="3">
								<a href="#" class="btn btn-primary" id="delete-history">Delete</a>
								&nbsp;&nbsp;&nbsp;
								<a href="#"  class="btn btn-danger" onClick="javascript:clearForm()">Reset</a>
							</td>
						</tr>
					</table>
				</form>
			</div>
			<!-- <div id="menu">
				<ul>
					<li><a href="admin-new-admin.jsp">ADMIN</a></li>
					<li><a href="admin-new-emp.jsp">EMPLOYEE</a></li>
					<li><a href="admin-new-supervisor.jsp">SUPERVISOR</a></li>
					<li><a href="admin-new-region.jsp">REGIONS</a></li>
					<li><a href="admin-new-holidays.jsp">REGIONAL HOLIDAYS</a></li>
					<li><a href="admin-new-history.jsp">HISTORY</a></li>
					<ul id="hisBar" style="list-style: none; margin-bottom: 0px; padding: 0 0 0 0;">
						<li><a href="admin-view-history.jsp">&nbsp;&nbsp; View Leave History</a></li>
						<li style="background:url('images/Arrow_Red.png');"><a href="admin-delete-history.jsp">&nbsp;&nbsp; Delete Leave History</a></li>
					</ul>
					<li><a href="admin-new-leave-request.jsp">LEAVE REQUESTS</a></li>
					<li><a href="admin-new-emp-leave-details.jsp">EMPLOYEES LEAVE DETAILS</a></li>
					<li><a href="admin-new-emp-leave-queue.jsp">PENDING LEAVE QUEUE</a></li>
					<li><a href="admin-new-mct-leave.jsp">MASTER CONCEPT LEAVE</a></li>
					<li><a href="admin-new-app-rej-req.jsp">APPROVE / REJECT REQUEST</a></li>
					<li><a href="admin-new-settings.jsp">SYSTEM SETTINGS</a></li>
					<li><a href="log-out.jsp">LOG OUT</a></li>
				</ul>                                                   
			</div> -->
	        <!-- <div class="push"></div> -->
	    </div>
	    </div>
	    <!-- <div class="footer"></div> -->
	    
	    <!-- Modal -->
			<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			    <h4 id="myModalLabel">Delete Leave History</h4>
			  </div>
			  <div class="modal-body">
			    <p>Please select at least one Leave History to delete.</p>
			  </div>
			  <div class="modal-footer">			    
			    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
			  </div>
			</div>
	</body>
</html>