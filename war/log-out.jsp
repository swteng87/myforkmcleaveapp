<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- 		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/> -->
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
	</head>
	<body>
		
	        <jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
			<jsp:setProperty name="jspbeans" property="*"/>
				<h5>Logout</h5>
				<jsp:forward page="/LogOut"/>
				<table cellpadding="5">
					<tr>
						<td>Logging out...</td>
					</tr>
				</table>
	      
	</body>
</html>