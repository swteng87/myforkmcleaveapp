package com.google.appengine.mct;

import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.datastore.DataStoreUtil;
import com.google.appengine.util.ConstantUtils;

public class LeaveEntitleService extends DataStoreUtil {

	private static final long serialVersionUID = 1L;
	
	public SickLeave getLessThanYear(){
		
		Filter filter = new FilterPredicate("sickLeaveType",
                FilterOperator.EQUAL,
                ConstantUtils.LESS_THAN);
		Query query = new Query(SickLeave.class.getSimpleName()).setFilter(filter);
		Entity entity = getDatastore().prepare(query).asSingleEntity();
			SickLeave sl = new SickLeave();
			sl.setRegion((String)entity.getProperty("region"));	
			sl.setLeaveEntitleId((String)entity.getProperty("leaveEntitleId"));
			sl.setSickLeaveDay((String)entity.getProperty("sickLeaveDay"));
			sl.setSickLeaveType((String)entity.getProperty("sickLeaveType"));
			sl.setSickLeaveYear((String)entity.getProperty("sickLeaveYear"));
			sl.setId(KeyFactory.keyToString(entity.getKey()));
		return sl;
		
	}
	
	public SickLeave getLessThanYearOrEqual(){
		
		Filter filter = new FilterPredicate("sickLeaveType",
                FilterOperator.EQUAL,
                ConstantUtils.LESS_THAN_OR_EQUAL);
		Query query = new Query(SickLeave.class.getSimpleName()).setFilter(filter);
		Entity entity = getDatastore().prepare(query).asSingleEntity();
			SickLeave sl = new SickLeave();
			sl.setRegion((String)entity.getProperty("region"));	
			sl.setLeaveEntitleId((String)entity.getProperty("leaveEntitleId"));
			sl.setSickLeaveDay((String)entity.getProperty("sickLeaveDay"));
			sl.setSickLeaveType((String)entity.getProperty("sickLeaveType"));
			sl.setSickLeaveYear((String)entity.getProperty("sickLeaveYear"));
			sl.setId(KeyFactory.keyToString(entity.getKey()));
		return sl;
		
	}
	
	public SickLeave getGreaterThan(){
		
		Filter filter = new FilterPredicate("sickLeaveType",
                FilterOperator.EQUAL,
                ConstantUtils.GREATER_THAN);
		Query query = new Query(SickLeave.class.getSimpleName()).setFilter(filter);
		Entity entity = getDatastore().prepare(query).asSingleEntity();
			SickLeave sl = new SickLeave();
			sl.setRegion((String)entity.getProperty("region"));	
			sl.setLeaveEntitleId((String)entity.getProperty("leaveEntitleId"));
			sl.setSickLeaveDay((String)entity.getProperty("sickLeaveDay"));
			sl.setSickLeaveType((String)entity.getProperty("sickLeaveType"));
			sl.setSickLeaveYear((String)entity.getProperty("sickLeaveYear"));
			sl.setId(KeyFactory.keyToString(entity.getKey()));
		return sl;
		
	}
	
	public SickLeave getGreaterThanOrEqual(){
		
		Filter filter = new FilterPredicate("sickLeaveType",
                FilterOperator.EQUAL,
                ConstantUtils.GREATER_THAN_OR_EQUAL);
		Query query = new Query(SickLeave.class.getSimpleName()).setFilter(filter);
		Entity entity = getDatastore().prepare(query).asSingleEntity();
			SickLeave sl = new SickLeave();
			sl.setRegion((String)entity.getProperty("region"));	
			sl.setLeaveEntitleId((String)entity.getProperty("leaveEntitleId"));
			sl.setSickLeaveDay((String)entity.getProperty("sickLeaveDay"));
			sl.setSickLeaveType((String)entity.getProperty("sickLeaveType"));
			sl.setSickLeaveYear((String)entity.getProperty("sickLeaveYear"));
			sl.setId(KeyFactory.keyToString(entity.getKey()));
		return sl;
		
	}
	
	public void addSickLeaveEntitle(String sickLeaveDay, String sickLeaveType,
			String sickLeaveYear, String region, String leaveEntitleId) {
		Entity entity = new Entity(SickLeave.class.getSimpleName());
		entity.setProperty("region", region);
		entity.setProperty("sickLeaveDay", sickLeaveDay);
		entity.setProperty("sickLeaveType", sickLeaveType);
		entity.setProperty("sickLeaveYear", sickLeaveYear);
		entity.setProperty("leaveEntitleId", leaveEntitleId);
		
		getDatastore().put(entity);
	}
	
	public void updateSickLeaveEntitle(String sickLeaveDay, String sickLeaveType,
			String sickLeaveYear, String region, String leaveEntitleId, String id){
		Filter filter = new FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
                FilterOperator.EQUAL,
                KeyFactory.stringToKey(id));
		Query query = new Query(SickLeave.class.getSimpleName()).setFilter(filter);
		Entity entity = getDatastore().prepare(query).asSingleEntity();
			Entity sickLeaveEntity = findEntityByKey(entity.getKey());
			sickLeaveEntity.setProperty("region", region);
			sickLeaveEntity.setProperty("sickLeaveDay", sickLeaveDay);
			sickLeaveEntity.setProperty("sickLeaveType", sickLeaveType);
			sickLeaveEntity.setProperty("sickLeaveYear", sickLeaveYear);
			sickLeaveEntity.setProperty("leaveEntitleId", leaveEntitleId);
			getDatastore().put(sickLeaveEntity);
		
	}
	
	
	public void deleteSickLeave(String id){
		Filter filter = new FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
                FilterOperator.EQUAL,
                KeyFactory.stringToKey(id));
		Query query = new Query(SickLeave.class.getSimpleName()).setFilter(filter);
		Entity entity = getDatastore().prepare(query).asSingleEntity();
		getDatastore().delete(entity.getKey());
		
			
	}
	
	public SickLeave getSickLeave(String id) {
		
		Filter filter = new FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
                FilterOperator.EQUAL,
                KeyFactory.stringToKey(id));
		Query query = new Query(SickLeave.class.getSimpleName()).setFilter(filter);
		Entity entity = getDatastore().prepare(query).asSingleEntity();
		SickLeave sl = new SickLeave();
		sl.setRegion((String)entity.getProperty("region"));	
		sl.setLeaveEntitleId((String)entity.getProperty("leaveEntitleId"));
		sl.setSickLeaveDay((String)entity.getProperty("sickLeaveDay"));
		sl.setSickLeaveType((String)entity.getProperty("sickLeaveType"));
		sl.setSickLeaveYear((String)entity.getProperty("sickLeaveYear"));
		sl.setId(KeyFactory.keyToString(entity.getKey()));
		
		return sl;
	}
	
	public List<SickLeave> getSickLeaveById(String id) {
		try {
			List<SickLeave> result = new ArrayList<SickLeave>();
		Filter filter = new FilterPredicate("leaveEntitleId",
                FilterOperator.EQUAL,
                id);
		Query query = new Query(SickLeave.class.getSimpleName()).setFilter(filter);
		for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
			SickLeave sl = new SickLeave();
			sl.setRegion((String)entity.getProperty("region"));	
			sl.setLeaveEntitleId((String)entity.getProperty("leaveEntitleId"));
			sl.setSickLeaveDay((String)entity.getProperty("sickLeaveDay"));
			sl.setSickLeaveType((String)entity.getProperty("sickLeaveType"));
			sl.setSickLeaveYear((String)entity.getProperty("sickLeaveYear"));
			sl.setId(KeyFactory.keyToString(entity.getKey()));
			result.add(sl);
		}
		return result;
		
		} catch (Exception e) {
			e.printStackTrace();
			 return null;
		}
	}
	

	public String addLeaveEntitle(String addAnnualLeave, String addMaternityLeave,
			String addBirthdayLeave, String addWeddingLeave, String addCompassionateLeave, String region,
			String compensationLeaveExp, String hospitalization) {
		Entity leaveEntitleEntity = new Entity(LeaveEntitle.class.getSimpleName());
		leaveEntitleEntity.setProperty("region", region);
		leaveEntitleEntity.setProperty("addAnnualLeave", addAnnualLeave);
		leaveEntitleEntity.setProperty("addMaternityLeave", addMaternityLeave);
		leaveEntitleEntity.setProperty("addBirthdayLeave", addBirthdayLeave);
		leaveEntitleEntity.setProperty("addWeddingLeave", addWeddingLeave);
		leaveEntitleEntity.setProperty("addCompassionateLeave", addCompassionateLeave);
		leaveEntitleEntity.setProperty("compensationLeaveExp", compensationLeaveExp);
		leaveEntitleEntity.setProperty("hospitalization", hospitalization);
		getDatastore().put(leaveEntitleEntity);
		return KeyFactory.keyToString(leaveEntitleEntity.getKey());   
	}
	
	public LeaveEntitle getLeaveEntitlebyRegion(String region) {
		try {
		Filter filter = new FilterPredicate("region",
                FilterOperator.EQUAL,
                region);
		Query query = new Query(LeaveEntitle.class.getSimpleName()).setFilter(filter);
		Entity entity = getDatastore().prepare(query).asSingleEntity();
		LeaveEntitle LeaveEntitleQ = new LeaveEntitle();		
		if(entity!=null){
			LeaveEntitleQ.setRegion((String)entity.getProperty("region"));		
			LeaveEntitleQ.setAddAnnualLeave((String)entity.getProperty("addAnnualLeave"));
			LeaveEntitleQ.setAddMaternityLeave((String)entity.getProperty("addMaternityLeave"));
			LeaveEntitleQ.setAddBirthdayLeave((String)entity.getProperty("addBirthdayLeave"));
			LeaveEntitleQ.setAddWeddingLeave((String)entity.getProperty("addWeddingLeave"));			
			LeaveEntitleQ.setAddCompassionateLeave((String)entity.getProperty("addCompassionateLeave"));
			LeaveEntitleQ.setCompensationLeaveExp((String)entity.getProperty("compensationLeaveExp"));
			LeaveEntitleQ.setHospitalization((String)entity.getProperty("hospitalization"));
			
			LeaveEntitleQ.setId(KeyFactory.keyToString(entity.getKey()));
		}
		return LeaveEntitleQ;
		
		} catch (Exception e) {
			e.printStackTrace();
			 return null;
		}
	}


	public List<LeaveEntitle> getLeaveEntitle() {
		Query query = new Query(LeaveEntitle.class.getSimpleName());
		List<LeaveEntitle> results = new ArrayList<LeaveEntitle>();
		for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
			LeaveEntitle LeaveEntitleQ = new LeaveEntitle();			
			LeaveEntitleQ.setRegion((String)entity.getProperty("region"));		
			LeaveEntitleQ.setAddAnnualLeave((String)entity.getProperty("addAnnualLeave"));
			LeaveEntitleQ.setAddMaternityLeave((String)entity.getProperty("addMaternityLeave"));
			LeaveEntitleQ.setAddBirthdayLeave((String)entity.getProperty("addBirthdayLeave"));
			LeaveEntitleQ.setAddWeddingLeave((String)entity.getProperty("addWeddingLeave"));			
			LeaveEntitleQ.setAddCompassionateLeave((String)entity.getProperty("addCompassionateLeave"));
			LeaveEntitleQ.setCompensationLeaveExp((String)entity.getProperty("compensationLeaveExp"));
			LeaveEntitleQ.setHospitalization((String)entity.getProperty("hospitalization"));
			LeaveEntitleQ.setId(KeyFactory.keyToString(entity.getKey()));
			results.add(LeaveEntitleQ);
		}
		
		return results;
	}
	
	public void deleteLeaveEntitle() {
		Query query = new Query(LeaveEntitle.class.getSimpleName());
		for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
			getDatastore().delete(entity.getKey());
			}
	}
		
	public void deleteLeaveEntitle(String region) {
		Transaction txn = getDatastore().beginTransaction();
		try {
			Query query = new Query(LeaveEntitle.class.getSimpleName());
			for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
				String tmp = (String)entity.getProperty("region");
				if (tmp.equalsIgnoreCase(region)) {
					getDatastore().delete(entity.getKey());
					txn.commit();
				}
			}
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}
	
	/*public void deleteLeaveEntitle(String region) {
		Query query = new Query(LeaveEntitle.class.getSimpleName());
		for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
			String tmp = (String)entity.getProperty("region");
			if (tmp.equalsIgnoreCase(region)) {
				getDatastore().delete(entity.getKey());
			}
		}
	}*/

	public String updateLeaveEntitle(String addAnnualLeave, String hospitalization, String addMaternityLeave,
			String addBirthdayLeave, String addWeddingLeave, String addCompassionateLeave, String region,
			String compensationLeaveExp) throws EntityNotFoundException {
		Filter filter = new FilterPredicate("region",
                FilterOperator.EQUAL,
                region);
		Query query = new Query(LeaveEntitle.class.getSimpleName()).setFilter(filter);
			Entity entity = getDatastore().prepare(query).asSingleEntity() ;
					Entity leaveEntitleEntity = getDatastore().get(entity.getKey());
					leaveEntitleEntity.setProperty("addAnnualLeave", addAnnualLeave);
					leaveEntitleEntity.setProperty("addMaternityLeave", addMaternityLeave);
					leaveEntitleEntity.setProperty("addBirthdayLeave", addBirthdayLeave);
					leaveEntitleEntity.setProperty("addWeddingLeave", addWeddingLeave);
					leaveEntitleEntity.setProperty("addCompassionateLeave", addCompassionateLeave);
					leaveEntitleEntity.setProperty("region", region);
					leaveEntitleEntity.setProperty("compensationLeaveExp", compensationLeaveExp);
					leaveEntitleEntity.setProperty("hospitalization", hospitalization);

					getDatastore().put(leaveEntitleEntity);
					return leaveEntitleEntity.getKey().toString();
	}
}
