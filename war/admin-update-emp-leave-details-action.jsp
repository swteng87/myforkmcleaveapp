<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>
<%@ page import="java.util.Calendar" %>
<%
String emailAddress = (String) request.getAttribute("emailAddress"); if (emailAddress == null) { emailAddress = ""; }

String year = (String) request.getAttribute("year"); if (year == null) { year = ""; }

String lastYearBal = (String) request.getAttribute("lastYearBal"); if (lastYearBal == null) { lastYearBal = ""; }

String entitledAnnual = (String) request.getAttribute("entitledAnnual"); if (entitledAnnual == null) { entitledAnnual = ""; }

String entitledComp = (String) request.getAttribute("entitledComp"); if (entitledComp == null) { entitledComp = ""; }

String noPayLeave = (String) request.getAttribute("noPayLeave"); if (noPayLeave == null) { noPayLeave = ""; }

String sickLeave = (String) request.getAttribute("sickLeave"); if (sickLeave == null) { sickLeave = ""; }

String annualLeave = (String) request.getAttribute("annualLeave"); if (annualLeave == null) { annualLeave = ""; }

String birthdayLeave = (String) request.getAttribute("birthdayLeave"); if (birthdayLeave == null) { birthdayLeave = ""; }

String compensationLeave = (String) request.getAttribute("compensationLeave"); if (compensationLeave == null) { compensationLeave = ""; }

String compassionateLeave = (String) request.getAttribute("compassionateLeave"); if (compassionateLeave == null) { compassionateLeave = ""; }

String maternityLeave = (String) request.getAttribute("maternityLeave"); if (maternityLeave == null) { maternityLeave = ""; }

String weddingLeave = (String) request.getAttribute("weddingLeave"); if (weddingLeave == null) { weddingLeave = ""; }

String others = (String) request.getAttribute("others"); if (others == null) { others = ""; }

String cri_region = (String) request.getAttribute("cri_region");

Calendar cal = Calendar.getInstance();
int cal_year = cal.get(Calendar.YEAR);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png"/>
    <style>
      undefined
    </style>
    <link rel="stylesheet" type="text/css" href="assets/css/datatable-bootstrap.css"/>
    <script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/datatable-bootstrap.js"></script>
	<script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
	<script src="assets/js/common.js"></script>
		<script language="JavaScript">
			var running = 0;
			function Go() {
				var f = document.forms[0];	
				if (f.emailAddress.value == "" || f.year.value == "" || f.lastYearBal.value == "" ||
					f.entitledAnnual.value == "" || f.entitledComp.value == "" ||
					f.noPayLeave.value == "" || f.sickLeave.value == "" || f.annualLeave.value == "" ||
					f.birthdayLeave.value == "" || f.compensationLeave.value == "" ||
					f.compassionateLeave.value == "" || f.maternityLeave.value == "" ||
					f.weddingLeave.value == "" || f.others.value == "" || f.cri_region.value == "" || f.cri_region.value == "Default") {
					/* alert("Please make sure the fields are filled."); */
					$('#myModal').modal('show')
					return false;
				}
				return true;
// 				f.submit();
// 				running++;
			}
			
  			function clearForm() {
  				var f_elements = document.forms[0].elements;
  				for (i = 0; i < f_elements.length; i++) {
    				field_type = f_elements[i].type.toLowerCase();
    				switch (field_type) {
    					case "text":
    					case "password":
						case "textarea":
    					case "hidden":
    						f_elements[i].value = "";
        					break;
    					case "radio":
    					case "checkbox":
        					if (f_elements[i].checked) {
            					f_elements[i].checked = false;
        					}
        					break;
    					case "select-one":
    						f_elements[i].selectedIndex = 0;
    						break;
    					case "select-multi":
        					f_elements[i].selectedIndex = -1;
        					break;
    					default:
        					break;
    				}
				}
  			}
		</script>
		<script type="text/javascript">
        $(document).ready(function () {
        	
        	$("#update-emp-leave-details").on("click",function(){
        		
        		
            	if(Go()){
            		$(".span9").block({
    	    		 	showOverlay: true, 
    	    	        centerY: true, 
    	    	        css: { 
    	    	            width: '200px',  
    	    	            border: 'none', 
    	    	            padding: '15px', 
    	    	            backgroundColor: '#000', 
    	    	            '-webkit-border-radius': '10px', 
    	    	            '-moz-border-radius': '10px', 
    	    	            opacity: .6, 
    	    	            color: '#fff' 
    	    	        }, 
    	    			message: '<font face="arial" size="4">Loading ...</font>'
    	    			
    	    		});
            		
            		var data = $("form").serialize();
            		
    	    		$.ajax({
    	    			type: "POST",
    	    			url: "/UpdateEmpLeaveDetailsAction",
    	    			data: data,
    	    			success: function(response) {
    	    				$(".span9").unblock();
    	                		
    	                		$("#msg").html(response);
    	                	
    	                		//window.location.href = "admin-update-emp.jsp";
    	                	
    	                	
    	                	return false;
    	    			}
    	    		});
            	}
            	
    				
            	});
        });
        	</script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
    <!-- <div class="well sidebar-nav"> -->
    <jsp:include page="leave-menu.jsp"></jsp:include>
    <!-- </div> -->
    </div>
    <div class="span9">
				<h5 id="Employees Leave Details">Update Employee Leave Details</h5>
				<hr></hr>
				<div id="msg" class="error-msg"></div>
				<form name="Update" method="post" action="UpdateEmpLeaveDetailsAction">
					<input type=hidden name=cmd value="">
					<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
					<jsp:setProperty name="jspbeans" property="*" />
					<table cellpadding="5" border="0" >
						<tr>
							<td align="left" width="160px">Email Address</td>
							<td width="250px">
								<div class="textbox">
									<input type="text" name="emailAddress" value="<%=emailAddress%>" maxlength="29" size="29"/>
								</div>
							</td>
							
						</tr>
						<tr>
							<td align="left">Year</td>
							<td>
								<select name="year">
									<%for(int i= cal_year; i>=1930; i--){ %>
									<option value="<%=i%>" <%= (Integer.parseInt(year)==0 ? 0 : Integer.parseInt(year)) == i ? "selected" : "" %>><%=i %></option>
									<%} %>
								</select>
							</td>
							
						</tr>
						<tr>
							<td align="left">Last Year's Balance</td>
							<td>
								<input type="text" name="lastYearBal" value="<%=lastYearBal%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">Entitled Annual Leave</td>
							<td>
								
								<input type="text" name="entitledAnnual" value="<%=entitledAnnual%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">Entitled Compensation Leave</td>
							<td>
								<input type="text" name="entitledComp" value="<%=entitledComp%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">Compensation Leave</td>
							<td>
								<input type="text" name="compensationLeave" value="<%=compensationLeave%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">Compassionate Leave</td>
							<td>
								<input type="text" name="compassionateLeave" value="<%=compassionateLeave%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">Sick Leave</td>
							<td>
								<input type="text" name="sickLeave" value="<%=sickLeave%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">Annual Leave</td>
							<td>
								<input type="text" name="annualLeave" value="<%=annualLeave%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">No Pay Leave</td>
							<td width="290px">
								<input type="text" name="noPayLeave" value="<%=noPayLeave%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">Birthday Leave</td>
							<td>
								<input type="text" name="birthdayLeave" value="<%=birthdayLeave%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">Maternity Leave</td>
							<td>
								<input type="text" name="maternityLeave" value="<%=maternityLeave%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">Wedding Leave</td>
							<td>
								<input type="text" name="weddingLeave" value="<%=weddingLeave%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left">Others</td>
							<td>
								<input type="text" name="others" value="<%=others%>" maxlength="4"/>
							</td>
							
						</tr>
						<tr>
							<td align="left" style="padding-left:0px;padding-bottom:15px;" width="40px">Region</td>
							<td>
							<div class="dropdown">
								<select name="cri_region">
									<%=jspbeans.listRegion()%>
								</select>
								<script>
									this.document.forms[0].cri_region.value="<%=cri_region%>"; 
								</script>
							</div>
							</td>
							<td>
							</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">
								<a href="#" id="update-emp-leave-details" class="btn btn-primary">Update</a>
								&nbsp;&nbsp;&nbsp;
								<a href="#" onClick="javascript:clearForm()" class="btn btn-danger">Reset</a>
								&nbsp;&nbsp;&nbsp;
<!-- 								<a href="admin-new-emp-leave-details.jsp"><img src="images/End_Button.png" style="border:none;" -->
<!-- 								onmouseover="this.src='images/End_Hover.png'" onmouseout="this.src='images/End_Button.png'"/></a> -->
							</td>
						</tr>
					</table>
				</form>
			</div>
			</div></div>
			
			<!-- Modal -->
			<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			    <h4 id="myModalLabel">Update Employee Leave Details</h4>
			  </div>
			  <div class="modal-body">
			    <p>Please make sure all fields are filled.</p>
			  </div>
			  <div class="modal-footer">			    
			    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
			  </div>
			</div>
	</body>
<html>