<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>
<%@page import="java.util.List"%>

<%
String emailAddress = (String) request.getAttribute("emailAddress"); if (emailAddress == null) { emailAddress = ""; }

String year = (String) request.getAttribute("year"); if (year == null) { year = ""; }

String lastYearBal = (String) request.getAttribute("lastYearBal"); if (lastYearBal == null) { lastYearBal = ""; }

String entitledAnnual = (String) request.getAttribute("entitledAnnual"); if (entitledAnnual == null) { entitledAnnual = ""; }

String entitledComp = (String) request.getAttribute("entitledComp"); if (entitledComp == null) { entitledComp = ""; }

String noPayLeave = (String) request.getAttribute("noPayLeave"); if (noPayLeave == null) { noPayLeave = ""; }

String sickLeave = (String) request.getAttribute("sickLeave"); if (sickLeave == null) { sickLeave = ""; }

String annualLeave = (String) request.getAttribute("annualLeave"); if (annualLeave == null) { annualLeave = ""; }

String birthdayLeave = (String) request.getAttribute("birthdayLeave"); if (birthdayLeave == null) { birthdayLeave = ""; }

String compensationLeave = (String) request.getAttribute("compensationLeave"); if (compensationLeave == null) { compensationLeave = ""; }

String compassionateLeave = (String) request.getAttribute("compassionateLeave"); if (compassionateLeave == null) { compassionateLeave = ""; }

String maternityLeave = (String) request.getAttribute("maternityLeave"); if (maternityLeave == null) { maternityLeave = ""; }

String weddingLeave = (String) request.getAttribute("weddingLeave"); if (weddingLeave == null) { weddingLeave = ""; }

List<String> myVec = (List<String>)request.getAttribute("myVec");

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script language="javaScript" type="text/javascript" src="script/calendar.js"></script>
   		<link href="css/calendar.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<script language="JavaScript">
			var running = 0;
			function Go() {
				var f = document.forms[0];	
				if (f.emailAddress.value == "" || f.year.value == "" || f.lastYearBal.value == "" ||
					f.entitledAnnual.value == "" || f.entitledComp.value == "" ||
					f.noPayLeave.value == "" || f.sickLeave.value == "" || f.annualLeave.value == "" ||
					f.birthdayLeave.value == "" || f.compensationLeave.value == "" ||
					f.compassionateLeave.value == "" || f.maternityLeave.value == "" ||
					f.weddingLeave.value == "") {
					alert("Please make sure the fields are filled.");
					return;
				}
				f.submit();
				running++;
			}
			function cmd_parm(msg) { 
				var f = document.forms[0];
				f.cmd2.value = msg;
				f.submit();
			}
			function toggle(id) {
    			var n = document.getElementById(id);
    			n.style.display =  (n.style.display != 'none' ? 'none' : '' );
  			}
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
	</head>
	<body>
		<div class="wrapper">
	        <div id="head"></div>
			<div id="content">
				<h4>ADD EMPLOYEE LEAVE DETAILS - IMPORT</h4>
				<br/>
				<form name="Import" method="post" action="ImportEmpLeaveDetails">
					<input type="hidden" name="cmd2" value="">
					<table cellpadding="5" style="color:black;" border="0">
						<tr bgcolor="#C0C0C0">
							<th width="24"></th>
							<th width="300" height="40" align="center" title="Documents"><img src="images/documents.png" width="45px"/></th>
						</tr>
						<%
						if(myVec != null){
							int index = 0;
							int bal = 0;
							for (int j=0; j<myVec.size(); j++) {
								int indexVec = 0;
								String tmpVecStr = myVec.get(j);
								indexVec = tmpVecStr.indexOf("|");
								String ssTitle = tmpVecStr.substring(0, indexVec);
								String ssHref = tmpVecStr.substring(indexVec+1, tmpVecStr.length());
								bal = index % 2;
								if (bal == 0) { %>
									<tr bgcolor="#FFF8C6">
								<% } else if (bal == 1) { %>
									<tr bgcolor="#F0F0F0">
								<% }%>
								<td align="center">
								<input type="radio" name="docRad"  value="<%=ssHref %>" onClick="javascript:cmd_parm()"/></td>
								<td><%=ssTitle %></td>
								</tr>
						<% index = index + 1; } }else {%>
						<tr><td colspan="10">No record</td></tr>
						<%} %>
						
					</table>
				</form>
			</div>
			<div id="menu">
				<ul>
					<li><a href="admin-new-admin.jsp">ADMIN</a></li>
					<li><a href="admin-new-emp.jsp">EMPLOYEE</a></li>
					<li><a href="admin-new-supervisor.jsp">SUPERVISOR</a></li>
					<li><a href="admin-new-region.jsp">REGIONS</a></li>
					<li><a href="admin-new-holidays.jsp">REGIONAL HOLIDAYS</a></li>
					<li><a href="admin-new-history.jsp">HISTORY</a></li>
					<li><a href="admin-new-leave-request.jsp">LEAVE REQUESTS</a></li>
					<li><a href="admin-new-emp-leave-details.jsp">EMPLOYEES LEAVE DETAILS</a></li>
					<ul id="eldBar" style="list-style: none; margin-bottom: 0px; padding: 0 0 0 0;">
						<li style="background:url('images/Arrow_Red.png');"><a href="admin-add-emp-leave-details.jsp">&nbsp;&nbsp; Add Employee Leave Details</a></li>
						<li><a href="admin-delete-emp-leave-details.jsp">&nbsp;&nbsp; Delete Employee Leave Details</a></li>
						<li><a href="admin-update-emp-leave-details.jsp">&nbsp;&nbsp; Update Employee Leave Details</a></li>
						<li><a href="admin-view-emp-leave-details.jsp">&nbsp;&nbsp; View Employees Leave Details</a></li>
					</ul>
					<li><a href="admin-new-emp-leave-queue.jsp">PENDING LEAVE QUEUE</a></li>
					<li><a href="admin-new-mct-leave.jsp">MASTER CONCEPT LEAVE</a></li>
					<li><a href="admin-new-app-rej-req.jsp">APPROVE / REJECT REQUEST</a></li>
					<li><a href="admin-new-settings.jsp">SYSTEM SETTINGS</a></li>
					<li><a href="log-out.jsp">LOG OUT</a></li>
				</ul>                                                   
			</div>
	        <div class="push"></div>
	    </div>
	    <div class="footer"></div>
	</body>
<html>