package com.google.appengine.mct;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.datatable.DataTableModel;
import com.google.appengine.datatable.DataTablesUtility;
import com.google.appengine.util.ConstantUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

@SuppressWarnings("serial")
public class ViewEmpLeaveDetails extends BaseServlet {
	
	private static final Logger log = Logger.getLogger(ViewEmpLeaveDetails.class);

	public static StringBuffer empDetailsListTable = new StringBuffer();
	public static StringBuffer selectedEmpDetails = new StringBuffer();
	public static StringBuffer transactionDetails = new StringBuffer();
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
	}

	public void doPostt(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		empDetailsListTable.setLength(0);
		selectedEmpDetails.setLength(0);
		transactionDetails.setLength(0);
		int bal = 0;
		int index = 0;
		int yearInt = 0;
		int monthInt = 0;
		int firstInd = 0;
		int lastInd = 0;
		String monthStr = "";
		String yearStr = "";
		String thisYear = "";
		String nextYear = "";
		String lastYear = "";
		Vector empVec = new Vector();
		empVec.setSize(0);
		String yearSelected = req.getParameter("cri_year");
		String regionSelected = req.getParameter("cri_region");
		String radioButton = req.getParameter("empDetRad");
		
		EmployeeLeaveDetailsService elds = new EmployeeLeaveDetailsService();
		
		if (radioButton != null) {
			int ind = 0;
			int digitYear = 0;
			int secondYear = 0;
			double balance = 0.0;
			
			String year = yearSelected;
			digitYear = Integer.parseInt(year);
			secondYear = digitYear + 1;
			for (EmployeeLeaveDetails eld : elds.getEmployeeLeaveDetails()) {
				if (eld.getId().equalsIgnoreCase(radioButton)) {
					if (eld.getYear().equalsIgnoreCase(year)) {

						transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
						transactionDetails.append("<td align=\"left\">");
						transactionDetails.append("Balance b/d ending 31 Mar " + digitYear);
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getLastYearBalance());
						transactionDetails.append("</td>");
						balance = Double.parseDouble(eld.getLastYearBalance());
						transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
						transactionDetails.append("<td>" + balance + "</td>");
						transactionDetails.append("<td></td>");
						transactionDetails.append("</tr>");
						transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
						transactionDetails.append("<td align=\"left\">");
						transactionDetails.append("From 1 Apr " + digitYear + " to 31 Mar " + secondYear);
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getEntitledAnnual());
						transactionDetails.append("</td>");
						balance = balance + Double.parseDouble(eld.getEntitledAnnual());
						transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
						transactionDetails.append("<td>" + balance + "</td>");
						transactionDetails.append("<td></td>");
						transactionDetails.append("</tr>");
						ApprovedLeaveService als = new ApprovedLeaveService();
						for (ApprovedLeave al : als.getApprovedLeave()) {
							if (al.getEmailAdd().equalsIgnoreCase(eld.getName())) {
								if (al.getLeaveType().equalsIgnoreCase("Compensation Leave Entitlement")) {
									String tmp = "";
									int indSpace = 0;
									indSpace = al.getTime().indexOf(" ");
									tmp = al.getTime().substring(0, indSpace);
									tmp = tmp.replace("/", "-");
									al.setStartDate(tmp);
									firstInd = al.getStartDate().indexOf("-");
									lastInd = al.getStartDate().lastIndexOf("-");
									monthStr = al.getStartDate().substring(firstInd+1, lastInd);
									yearStr = al.getStartDate().substring(lastInd+1, al.getStartDate().length());
									monthInt = Integer.parseInt(monthStr);
									thisYear = yearSelected;
									yearInt = Integer.parseInt(yearStr);
									lastYear = Integer.toString(yearInt - 1);
									nextYear = Integer.toBinaryString(yearInt + 1);
								} else {
									firstInd = al.getStartDate().indexOf("-");
									lastInd = al.getStartDate().lastIndexOf("-");
									monthStr = al.getStartDate().substring(firstInd+1, lastInd);
									yearStr = al.getStartDate().substring(lastInd+1, al.getStartDate().length());
									monthInt = Integer.parseInt(monthStr);
									thisYear = yearSelected;
									yearInt = Integer.parseInt(yearStr);
									lastYear = Integer.toString(yearInt - 1);
									nextYear = Integer.toBinaryString(yearInt + 1);
								}
								if (monthInt <= 3) {
									if (yearSelected.equalsIgnoreCase(lastYear)) {
										if (al.getLeaveType().equalsIgnoreCase("Compensation Leave Entitlement")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											balance = balance + Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\">" + al.getRemark() + "</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Sick Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td>");
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("No Pay Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Annual Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											balance = balance - Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td></td><td></td>");
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Compensation Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											balance = balance - Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td></td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Compassionate Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Compassionate Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Birthday Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Birthday Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Maternity Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Maternity Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Wedding Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Wedding Leave</td>");
											transactionDetails.append("</tr>");
										}
									}
								}
								else {
									if (al.getStartDate().contains(yearSelected)) {
										if (al.getLeaveType().equalsIgnoreCase("Compensation Leave Entitlement")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											balance = balance + Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\">" + al.getRemark() + "</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Sick Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td>");
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("No Pay Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Annual Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											balance = balance - Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td></td><td></td>");
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Compensation Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											balance = balance - Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td></td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Compassionate Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Compassionate Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Birthday Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Birthday Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Maternity Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Maternity Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Wedding Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Wedding Leave</td>");
											transactionDetails.append("</tr>");
										}
									}
								}
							}
						}
						transactionDetails.append("<tr bgcolor=\"#FFF8C6\" align=\"center\">");
						transactionDetails.append("<td align=\"right\">Total</td>");
						transactionDetails.append("<td>");
						double entitledTotal = 0.0;
						double others = 0.0;
						System.out.println("Last year balance = " + eld.getLastYearBalance());
						System.out.println("Entitled Annual = " + eld.getEntitledAnnual());
						System.out.println("Entitled Compensation = " + eld.getEntitledCompensation());
						entitledTotal = (Double.parseDouble(eld.getLastYearBalance())) + (Double.parseDouble(eld.getEntitledAnnual())) + (Double.parseDouble(eld.getEntitledCompensation()));
						others = (Double.parseDouble(eld.getCompassionateLeave())) + (Double.parseDouble(eld.getBirthdayLeave())) + (Double.parseDouble(eld.getMaternityLeave())) + (Double.parseDouble(eld.getWeddingLeave()));
						transactionDetails.append(entitledTotal);
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getNoPayLeave());
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getSickLeave());
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getAnnualLeave());
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getCompensationLeave());
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(others);
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						eld.setBalance(Double.toString(balance));
						transactionDetails.append(eld.getBalance());
						transactionDetails.append("</td>");
						transactionDetails.append("<td></td>");
						transactionDetails.append("</tr>");
						
						selectedEmpDetails.append("<tr>");
						selectedEmpDetails.append("<td width=\"190px\">Name</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getName() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Year</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getYear() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Last Year's Balance</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getLastYearBalance() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Entitled Annual</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getEntitledAnnual() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Entitled Compensation</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getEntitledCompensation() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Annual Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getAnnualLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Sick Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getSickLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Birthday Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getBirthdayLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>No Pay Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getNoPayLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Compensation Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getCompensationLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Compassionate Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getCompassionateLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Maternity Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getMaternityLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Wedding Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getWeddingLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Balance</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getBalance() + " Days" + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("</tr><tr><td colspan=\"2\"><hr></td></tr>");
					}
				}
			}
			try {
				getServletConfig().getServletContext().getRequestDispatcher("/admin-view-emp-leave-details-action.jsp").forward(req, resp);
				return;
			} catch (ServletException e) {
				log.error("ViewEmpLeaveDetails * doPost - error: " + e.getMessage());
				e.printStackTrace();
			}
		} else {
			req.setAttribute("cri_year", yearSelected);
			req.setAttribute("cri_region", regionSelected);
			try {
				getServletConfig().getServletContext().getRequestDispatcher("/admin-view-emp-leave-details.jsp").forward(req, resp);
				return;
			} catch (ServletException e) {
				log.error("ViewEmpLeaveDetails * doPost - error: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}


	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		log.debug(ViewEmpLeaveDetails.class);
		String yearSelected = req.getParameter("year");
		String viewId = req.getParameter("view");
		String isAdmin = ConstantUtils.FALSE;
		AdministratorService addService = new AdministratorService();
		String eAdd = (String)req.getSession().getAttribute("emailAdd");
		Administrator admin = addService.findAdministratorByEmailAddress(eAdd);
		if(StringUtils.isNotBlank(admin.getEmailAddress())){
			 isAdmin = ConstantUtils.TRUE; 
		}else{
			isAdmin = ConstantUtils.FALSE;
		}
		
		req.setAttribute("isAdmin", isAdmin);
		
		EmployeeLeaveDetailsService elds = new EmployeeLeaveDetailsService();
		ApprovedLeaveService als = new ApprovedLeaveService();
		for (EmployeeLeaveDetails eld : elds.getEmployeeLeaveDetails()) {
			if (eld.getId().equalsIgnoreCase(viewId)) {
					req.setAttribute("fullName", eld.getName());
				}
			
		}
		Integer lastYear = 0;
		SimpleDateFormat standardDF = new SimpleDateFormat(ConstantUtils.DATE_FORMAT);
		Integer currentYear = 0;
		Integer nextYear = 0;
		
		String year = yearSelected;
		currentYear = Integer.parseInt(year);
		lastYear = currentYear - 1;
		nextYear = currentYear + 1;
			
			EmployeeLeaveDetails eld = elds.findEmployeeLeaveDetailsByValue(Entity.KEY_RESERVED_PROPERTY,viewId,ConstantUtils.EQUAL);
			req.setAttribute("eld", eld);
			
			List<ApprovedLeave> appList = als.getApproveLeaveListByEmail(eld.getEmailAddress());
			List<ApprovedLeave> newAppList = new ArrayList<ApprovedLeave>();
			if(appList != null && !appList.isEmpty()){
				for(ApprovedLeave approvedLeave : appList){
					Calendar currMonth = Calendar.getInstance(Locale.getDefault());
					if(!ConstantUtils.COMPENSATION_LEAVE_ENTITLEMENT.equals(approvedLeave.getLeaveType())){
						try {
							currMonth.setTime(standardDF.parse(approvedLeave.getStartDate()));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if((currMonth.get(Calendar.MONTH) > 2 && 
								currMonth.get(Calendar.YEAR) == Integer.parseInt(yearSelected))
								|| (currMonth.get(Calendar.MONTH) <= 2 && 
										currMonth.get(Calendar.YEAR) == Integer.parseInt(yearSelected)+1)
										|| (currMonth.get(Calendar.MONTH) <= 2 && 
												currMonth.get(Calendar.YEAR)+1 == Integer.parseInt(yearSelected)+1)){
							ApprovedLeave appLeave = new ApprovedLeave();
							appLeave.setTime(approvedLeave.getTime());
							appLeave.setEmailAdd(approvedLeave.getEmailAdd());
							appLeave.setNumOfDays(approvedLeave.getNumOfDays());
							appLeave.setStartDate(approvedLeave.getStartDate());
							appLeave.setEndDate(approvedLeave.getEndDate());
							appLeave.setLeaveType(approvedLeave.getLeaveType());
							appLeave.setSupervisor(approvedLeave.getSupervisor());
							appLeave.setRemark(approvedLeave.getRemark());
							appLeave.setRegion(approvedLeave.getRegion());
							appLeave.setChangeType(approvedLeave.getChangeType());
							appLeave.setAttachmentUrl(approvedLeave.getAttachmentUrl());
							appLeave.setId(approvedLeave.getId());
							newAppList.add(appLeave);
						}
					}
					else{
						try {
							currMonth.setTime(standardDF.parse(approvedLeave.getTime()));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if((currMonth.get(Calendar.MONTH) > 2 && 
								currMonth.get(Calendar.YEAR) == Integer.parseInt(yearSelected))
								|| (currMonth.get(Calendar.MONTH) <= 2 && 
										currMonth.get(Calendar.YEAR) == Integer.parseInt(yearSelected)+1)
										|| (currMonth.get(Calendar.MONTH) <= 2 && 
											currMonth.get(Calendar.YEAR)+1 == Integer.parseInt(yearSelected)+1)){
							ApprovedLeave appLeave = new ApprovedLeave();
							appLeave.setTime(approvedLeave.getTime());
							appLeave.setEmailAdd(approvedLeave.getEmailAdd());
							appLeave.setNumOfDays(approvedLeave.getNumOfDays());
							appLeave.setStartDate(approvedLeave.getStartDate());
							appLeave.setEndDate(approvedLeave.getEndDate());
							appLeave.setLeaveType(approvedLeave.getLeaveType());
							appLeave.setSupervisor(approvedLeave.getSupervisor());
							appLeave.setRemark(approvedLeave.getRemark());
							appLeave.setRegion(approvedLeave.getRegion());
							appLeave.setChangeType(approvedLeave.getChangeType());
							appLeave.setAttachmentUrl(approvedLeave.getAttachmentUrl());
							appLeave.setProjectName(approvedLeave.getProjectName());
							appLeave.setId(approvedLeave.getId());
							newAppList.add(appLeave);
						}
						
					}
				}
			}
			
			req.setAttribute("appList", newAppList);
			
			Double entitledTotal = 0.0;
			Double others = 0.0;
			String lastYearBalance = eld.getLastYearBalance() == null ? "0" : eld.getLastYearBalance();
			String entitledAnnual= eld.getEntitledAnnual() == null ? "0" : eld.getEntitledAnnual();
			String entitledCompensation = eld.getEntitledCompensation()  == null ? "0" : eld.getEntitledCompensation();
			entitledTotal = (Double.parseDouble(lastYearBalance)) + 
					(Double.parseDouble(entitledAnnual)) + 
					(Double.parseDouble(entitledCompensation));
			
			String compassionateLeave = eld.getCompassionateLeave() == null ? "0" : eld.getCompassionateLeave();
			String birthdayLeave = eld.getBirthdayLeave() == null ? "0" : eld.getBirthdayLeave();
			String maternityLeave = eld.getMaternityLeave() == null ? "0" : eld.getMaternityLeave();
			String weddingLeave = eld.getWeddingLeave()  == null ? "0" : eld.getWeddingLeave(); 
			
			others = (Double.parseDouble(compassionateLeave)) + 
					(Double.parseDouble(birthdayLeave)) + 
					(Double.parseDouble(maternityLeave)) + 
					(Double.parseDouble(weddingLeave));
			
			req.setAttribute("yearSelected", yearSelected);
			req.setAttribute("lastYear", lastYear.toString());
			req.setAttribute("entitledTotal", entitledTotal.toString());
			req.setAttribute("others", others.toString());
			req.setAttribute("currentYear", currentYear.toString());
			req.setAttribute("nextYear", nextYear.toString());
			
			EmployeeService els = new EmployeeService();
			Employee employee = els.findEmployeeByColumnName("emailAddress", eld.getEmailAddress());
			Calendar cal = Calendar.getInstance(Locale.getDefault());
			Calendar curr = Calendar.getInstance(Locale.getDefault());
       		try {
				cal.setTime(standardDF.parse(employee.getHiredDate()));
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
       		int yearCal = curr.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
			LeaveEntitleService les = new LeaveEntitleService();
			int allowSickLeave = 0;
			LeaveEntitle le = les.getLeaveEntitlebyRegion(eld.getRegion());
				List<SickLeave> sickLeaveList = les.getSickLeaveById(le.getId());
				
				for(SickLeave sl : sickLeaveList){
					if(ConstantUtils.LESS_THAN.equals(sl.getSickLeaveType())){
						if(yearCal < Integer.parseInt(sl.getSickLeaveYear())){
							allowSickLeave = Integer.parseInt(sl.getSickLeaveDay());
						}
					}
					else if(ConstantUtils.LESS_THAN_OR_EQUAL.equals(sl.getSickLeaveType())){
						SickLeave lessThan = les.getLessThanYear(le.getId());
						if(lessThan != null ){
								if(yearCal <= Integer.parseInt(sl.getSickLeaveYear()) &&
										yearCal >= Integer.parseInt(lessThan.getSickLeaveYear())){
									allowSickLeave = Integer.parseInt(sl.getSickLeaveDay());
								}
						}
						
					}
					else if(ConstantUtils.GREATER_THAN.equals(sl.getSickLeaveType())){
						if(yearCal > Integer.parseInt(sl.getSickLeaveYear())){
							allowSickLeave = Integer.parseInt(sl.getSickLeaveDay());
						}
					}
					else if(ConstantUtils.GREATER_THAN_OR_EQUAL.equals(sl.getSickLeaveType())){
						if(yearCal >= Integer.parseInt(sl.getSickLeaveYear())){
							allowSickLeave = Integer.parseInt(sl.getSickLeaveDay());
						}
					}
				}
			
			
				req.setAttribute("allowSickLeave", String.valueOf(allowSickLeave));
				
			try {
				getServletConfig().getServletContext().getRequestDispatcher("/admin-view-emp-leave-details-action.jsp").forward(req, resp);
				return;
			} catch (ServletException e) {
				log.error("ViewEmpLeaveDetails * doPost - error: " + e.getMessage());
				e.printStackTrace();
			}
	
	}
	


	public String empDetailsListTableSb() {
		if (empDetailsListTable.length() == 0) {
			empDetailsListTable.append("<tr bgcolor=\"#FFF8C6\">");
			empDetailsListTable.append("<td colspan=\"4\">" + " " + "</td>");
			empDetailsListTable.append("</tr>");
			empDetailsListTable.append("<tr bgcolor=\"#F0F0F0\">");
			empDetailsListTable.append("<td colspan=\"4\">" + "There are no records in the database for this region." + "</td>");
			empDetailsListTable.append("</tr>");
		}
		return empDetailsListTable.toString();
	}


	public String selectedEmpDetailsSb() {
		if (selectedEmpDetails.length() == 0) {
			selectedEmpDetails.append("<tr bgcolor=\"#FFF8C6\">");
			selectedEmpDetails.append("<td colspan=\"4\">" + " " + "</td>");
			selectedEmpDetails.append("</tr>");
			selectedEmpDetails.append("<tr bgcolor=\"#F0F0F0\">");
			selectedEmpDetails.append("<td colspan=\"4\">" + "There are no records in the database for this region." + "</td>");
			selectedEmpDetails.append("</tr>");
		}
		return selectedEmpDetails.toString();
	}


	public String transactionDetailsSb() {
		return transactionDetails.toString();
	}
}
