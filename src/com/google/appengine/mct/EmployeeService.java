package com.google.appengine.mct;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.datastore.DataStoreUtil;

public class EmployeeService extends DataStoreUtil {

	private static final long serialVersionUID = 1L;
	
	public Employee findEmployeeByColumnName(String columnName, String value){
		Entity entity =  findEntities(Employee.class.getSimpleName(),columnName, value);
		Employee employee = new Employee();
		if(entity != null){
			employee.setEmailAddress((String)entity.getProperty("emailAddress"));
			employee.setFullName((String)entity.getProperty("fullName"));
			employee.setRegion((String)entity.getProperty("region"));
			employee.setHiredDate((String)entity.getProperty("hiredDate"));
			employee.setBirthDate((String)entity.getProperty("birthDate"));
			employee.setResignedDate((String)entity.getProperty("resignedDate"));
			employee.setSupervisor((String)entity.getProperty("supervisor"));
			employee.setJobTitle((String)entity.getProperty("jobTitle"));
			employee.setId(KeyFactory.keyToString(entity.getKey()));
		}
		
		return employee;
	}

	public String addEmployee(String emailAddress, String fullName, String region, 
			String hiredDate, String birthDate, String resignedDate, String supervisor, String jobTitle) {
		Key employeeKey = KeyFactory.createKey(Employee.class.getSimpleName(), "employee");
		Entity employeeEntity = new Entity(Employee.class.getSimpleName(),employeeKey);
		employeeEntity.setProperty("emailAddress", emailAddress);
		employeeEntity.setProperty("fullName", fullName);
		employeeEntity.setProperty("region", region);
		employeeEntity.setProperty("hiredDate", hiredDate);
		employeeEntity.setProperty("birthDate", birthDate);
		employeeEntity.setProperty("resignedDate", resignedDate);
		employeeEntity.setProperty("supervisor", supervisor);
		employeeEntity.setProperty("jobTitle", jobTitle);
		getDatastore().put(employeeEntity);
		return KeyFactory.keyToString(employeeEntity.getKey());   
	}


	public List<Employee> getEmployees() {
		Query query = new Query(Employee.class.getSimpleName());
		List<Employee> results = new ArrayList<Employee>();
		for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
			Employee employee = new Employee();
			employee.setEmailAddress((String)entity.getProperty("emailAddress"));
			employee.setFullName((String)entity.getProperty("fullName"));
			employee.setRegion((String)entity.getProperty("region"));
			employee.setHiredDate((String)entity.getProperty("hiredDate"));
			employee.setBirthDate((String)entity.getProperty("birthDate"));
			employee.setResignedDate((String)entity.getProperty("resignedDate"));
			employee.setSupervisor((String)entity.getProperty("supervisor"));
			employee.setJobTitle((String)entity.getProperty("jobTitle"));
			employee.setId(KeyFactory.keyToString(entity.getKey()));
			results.add(employee);
		}

		/* Sort by region */
		Collections.sort(results, new SortByRegion());
		return results;
	}


	public void deleteEmployee(String emailAddress) {
			Query query = new Query(Employee.class.getSimpleName());
			for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
				String tmp = (String)entity.getProperty("emailAddress");
				if (tmp.equalsIgnoreCase(emailAddress)) {
					getDatastore().delete(entity.getKey());
				}
			}
	}


	public void updateEmployee(String emailAddress, String fullName, String region, 
			String hiredDate, String birthDate, String resignedDate, String supervisor, String jobTitle) throws EntityNotFoundException {

			Query query = new Query(Employee.class.getSimpleName());
			for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
				String tmp = (String)entity.getProperty("emailAddress");
				if (tmp.equalsIgnoreCase(emailAddress)) {
					Entity employee = getDatastore().get(entity.getKey());
					employee.setProperty("emailAddress", emailAddress);
					employee.setProperty("fullName", fullName);
					employee.setProperty("region", region);
					employee.setProperty("hiredDate", hiredDate);
					employee.setProperty("birthDate", birthDate);
					employee.setProperty("resignedDate", resignedDate);
					employee.setProperty("supervisor", supervisor);
					employee.setProperty("jobTitle", jobTitle);
					getDatastore().put(employee);
				}
			}
	}
	
}
