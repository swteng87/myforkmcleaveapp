		<ul class="nav nav-list bs-docs-sidenav">
		  <li class="dropdown-submenu">
    		<a tabindex="-1" href="#"><i class="icon-play pull-right"></i>Leave Requests Admin</a>
    		<ul class="dropdown-menu">
      			<li class="side-menu"><a tabindex="-1" href="admin-approved.jsp">Approved Requests</a></li>      
     			<li class="side-menu"><a href="admin-rejected.jsp">Rejected Requests</a></li>
      			
    		</ul>
  		  </li>
  		  <li class="side-menu"><a href="admin-delete-emp-leave-details.jsp"><i class="icon-play pull-right"></i>Employees Leave Details</a></li>
  		  <li class="side-menu"><a href="admin-delete-emp-leave-queue.jsp"><i class="icon-play pull-right"></i>Pending Leave Queue</a></li>
  		  <li class="side-menu"><a href="admin-emp-leave-form.jsp"><i class="icon-play pull-right"></i>Leave Application Form</a></li>
     	  <li class="side-menu"><a href="admin-comp-leave-form.jsp"><i class="icon-play pull-right"></i>Comp Leave Entitlement Form</a></li>
     	  <li class="side-menu"><a href="admin-leave-amend.jsp"><i class="icon-play pull-right"></i>Amend / Cancel Leave</a></li>
  		  <!-- <li class="dropdown-submenu">
    		<a tabindex="-1" href="#"><i class="icon-play pull-right"></i>MASTER CONCEPT LEAVE</a>
    		<ul class="dropdown-menu">
      			<li class="side-menu"><a tabindex="-1" href="admin-emp-policy.jsp">Master Concept Leave Policy</a></li>      
     			<li class="side-menu"><a href="admin-emp-leave-form.jsp">Leave Application Form</a></li>
     			<li class="side-menu"><a href="admin-comp-leave-form.jsp">Compensation Leave Entitlement Form</a></li>
     			<li class="side-menu"><a href="admin-leave-amend.jsp">Amend / Cancel Leave</a></li>      			
    		</ul>
  		  </li> -->
  		  <li class="side-menu"><a href="admin-request-action.jsp"><i class="icon-play pull-right"></i>Approve / Reject Request</a></li>
  		            
          <li class="side-menu"><a href="log-out.jsp">Logout</a></li>
        </ul>