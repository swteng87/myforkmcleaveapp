<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<%
String emailAddress = (String) request.getAttribute("emailAddress"); if (emailAddress == null) { emailAddress = ""; }

String fullName = (String) request.getAttribute("fullName"); if (fullName == null) { fullName = ""; }
	
String region = (String) request.getAttribute("region"); if (region == null) { region = ""; }
	
String hiredDate = (String) request.getAttribute("hiredDate"); if (hiredDate == null) { hiredDate = ""; }
	
String birthDate = (String) request.getAttribute("birthDate"); if (birthDate == null) { birthDate = ""; }

String resignedDate = (String) request.getAttribute("resignedDate"); if (resignedDate == null) { resignedDate = ""; }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script language="javaScript" type="text/javascript" src="script/calendar.js"></script>
   		<link href="css/calendar.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<script language="JavaScript">
			var running = 0;
			function Go() {
				var f = document.forms[0];	
				if (f.emailAddress.value == "" || f.fullName.value == "" || f.region.value == "" || f.birthDate.value == "") {
					alert("Please make sure all fields are filled.");
					return;
				}
				f.submit();
				running++;
			}
			function cmd_parm(msg) { 
				var f = document.forms[0];
				f.cmd2.value = msg;
				if (f.cmd2.value == "End"){
					if (f.emailAddress.value == "" || f.fullName.value == "" || f.region.value == "" || f.birthDate.value == "") {
						alert("Please make sure the field is filled.");
						return;
					}
				}
				f.submit();
			}
			function toggle(id) {
    			var n = document.getElementById(id);
    			n.style.display =  (n.style.display != 'none' ? 'none' : '' );
  			}
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
	</head>
	<body>
		<div class="wrapper">
	        <div id="head"></div>
			<div id="content">
				<h4>ADD EMPLOYEE - IMPORT FILE</h4>
				<br/>
				<form name="Import" method="post" action="ImportEmployee">
					<input type="hidden" name="cmd2" value="">
					<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
					<jsp:setProperty name="jspbeans" property="*" />
					<table cellpadding="5" style="color:black;" border="0">
						<tr bgcolor="#C0C0C0">
							<th width="24"></th>
							<th width="300" height="40" align="center" title="Documents"><img src="images/documents.png" width="45px"/></th>
						</tr>
						<%=jspbeans.listDocumentsSb()%>
					</table>
				</form>
			</div>
			<div id="menu">
				<ul>
					<li><a href="admin-new-admin.jsp">ADMIN</a></li>
					<li><a href="admin-new-emp.jsp">EMPLOYEE</a></li>
					<ul id="empBar" style="list-style: none; margin-bottom: 0px; padding: 0 0 0 0;">
						<li style="background:url('images/Arrow_Red.png');"><a href="admin-add-emp.jsp">&nbsp;&nbsp; Add Employee</a></li>
						<li><a href="admin-view-emp.jsp">&nbsp;&nbsp; View Employee</a></li>
						<li><a href="admin-delete-emp.jsp">&nbsp;&nbsp; Delete Employee</a></li>
						<li><a href="admin-update-emp.jsp">&nbsp;&nbsp; Update Employee</a></li>
					</ul>
					<li><a href="admin-new-supervisor.jsp">SUPERVISOR</a></li>
					<li><a href="admin-new-region.jsp">REGIONS</a></li>
					<li><a href="admin-new-holidays.jsp">REGIONAL HOLIDAYS</a></li>
					<li><a href="admin-new-history.jsp">HISTORY</a></li>
					<li><a href="admin-new-leave-request.jsp">LEAVE REQUESTS</a></li>
					<li><a href="admin-new-emp-leave-details.jsp">EMPLOYEES LEAVE DETAILS</a></li>
					<li><a href="admin-new-emp-leave-queue.jsp">PENDING LEAVE QUEUE</a></li>
					<li><a href="admin-new-mct-leave.jsp">MASTER CONCEPT LEAVE</a></li>
					<li><a href="admin-new-app-rej-req.jsp">APPROVE / REJECT REQUEST</a></li>
					<li><a href="admin-new-settings.jsp">SYSTEM SETTINGS</a></li>
					<li><a href="log-out.jsp">LOG OUT</a></li>
				</ul>                                                   
			</div>
	        <div class="push"></div>
	    </div>
	    <div class="footer"></div>
	</body>
<html>