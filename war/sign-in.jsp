<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.google.appengine.mct.*"%>

<%
String emailAdd = (String) request.getAttribute("emailAdd"); 
	if (emailAdd == null) { 
		emailAdd = ""; 
	}

String password = (String) request.getAttribute("password"); 
	if (password == null) { 
		password = ""; 
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<script language="JavaScript">
			browser_version = parseInt(navigator.appVersion);
			browser_type = navigator.appName;
			
			var nVer = navigator.appVersion;
			var nAgt = navigator.userAgent;
			var browserName  = navigator.appName;
			var fullVersion  = ''+parseFloat(navigator.appVersion); 
			var majorVersion = parseInt(navigator.appVersion,10);
			var nameOffset,verOffset,ix;

			// In Opera, the true version is after "Opera" or after "Version"
			if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
			 browserName = "Opera";
			 fullVersion = nAgt.substring(verOffset+6);
			 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
			   fullVersion = nAgt.substring(verOffset+8);
			}
			// In MSIE, the true version is after "MSIE" in userAgent
			else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
			 browserName = "Microsoft Internet Explorer";
			 fullVersion = nAgt.substring(verOffset+5);
			}
			// In Chrome, the true version is after "Chrome" 
			else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
			 browserName = "Chrome";
			 fullVersion = nAgt.substring(verOffset+7);
			}
			// In Safari, the true version is after "Safari" or after "Version" 
			else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
			 browserName = "Safari";
			 fullVersion = nAgt.substring(verOffset+7);
			 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
			   fullVersion = nAgt.substring(verOffset+8);
			}
			// In Firefox, the true version is after "Firefox" 
			else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
			 browserName = "Firefox";
			 fullVersion = nAgt.substring(verOffset+8);
			}
			// In most other browsers, "name/version" is at the end of userAgent 
			else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
					  (verOffset=nAgt.lastIndexOf('/')) ) 
			{
			 browserName = nAgt.substring(nameOffset,verOffset);
			 fullVersion = nAgt.substring(verOffset+1);
			 if (browserName.toLowerCase()==browserName.toUpperCase()) {
			  browserName = navigator.appName;
			 }
			}
			// trim the fullVersion string at semicolon/space if present
			if ((ix=fullVersion.indexOf(";"))!=-1)
			   fullVersion=fullVersion.substring(0,ix);
			if ((ix=fullVersion.indexOf(" "))!=-1)
			   fullVersion=fullVersion.substring(0,ix);

			majorVersion = parseInt(''+fullVersion,10);
			if (isNaN(majorVersion)) {
			 fullVersion  = ''+parseFloat(navigator.appVersion); 
			 majorVersion = parseInt(navigator.appVersion,10);
			}
			
			if (browser_type == "Microsoft Internet Explorer" && (browser_version >= 4)) {
			document.write("<link rel='stylesheet' href='css/ie-style.css' type='text/css' media='screen'>");
			}
			else if (browser_type == "Netscape" && (browser_version >= 4)) {
			document.write("<link rel='stylesheet' href='css/firefox-style.css' type='text/css' media='screen'>");
			}
			if (browserName == "Chrome") {
			document.write("<link rel='stylesheet' href='css/chrome-style.css' type='text/chrome/safari' media='screen'>");
			}
			
			var running = 0;
			function cmd() { 
				var f = document.forms[0];
				f.hd.value = "hkmci.com";
				f.submit();
			}
			
			function Go() {
				var f = document.forms[0];	
				if (f.emailAdd.value == "" || f.password.value == "") {
					alert("Please enter your Email Address and Password!");
					return;
				}
				f.submit();
				running++;
			}
		</script>
	</head>
	<body>
		<div id="bg">
<!-- 			<img src="images/Login.jpg"> -->
			<img src="images/LoginPro.jpg">
		</div>
		<div style="z-index:2; position:relative; margin:0px; padding:0px; height:100%; width:100%; overflow:scroll;">
			<p>&nbsp;</p>
			<div>
<!-- 				<form name="Login" method="post" action="SignIn"> -->
<!-- 				<input type="hidden" name="cmd2" value=""> -->
<!-- 					<div id="user"> -->
<%-- 						<input type="text" name="emailAdd" value="<%=emailAdd%>" maxlength="40" class="textA" style="border: 0px solid #000000; background-color: transparent; background-image: url('transparent.gif'); outline:none;"/> --%>
<!-- 					</div> -->
<!-- 					<div id="pwd"> -->
<%-- 						<input type="password" name="password" value="<%=password%>" style="height:30px; width:400px; border: 0px solid #000000; background-color: transparent; background-image: url('transparent.gif'); outline:none;" maxlength="40"/> --%>
<!-- 					</div> -->
<!-- 					<div id="enter"> -->
<!-- 						<input type="image" src="images/LoginButton.png" height="50" style="border:none;"> -->
						
<!-- 					</div> -->
<!-- 				</form> -->

<!-- production login -->
        <form action="<c:url value="/sso"/>" method="get">
        <input type="hidden" name="hd" value="">
        <div id="user">
           			<table>
							<tr>
								<td>
									<font style="color:white;font-family:Tahoma;font-size:14px;">&nbsp;Sign in using:</font>
								</td>
								<td>
									 <input type="image" src="images/Google.ico" style="border:none;" height="50" onClick="javascript:cmd()">
								</td>
							</tr>
						</table>
            </div>
        </form>
			</div>
		</div>
	</body>	
</html>